import {Component, Input, OnInit, Output} from '@angular/core';
import {QuestionModel} from '../model/questionModel';
import {QuizzAPIService} from '../service/quizzAPIService';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  show = false;
  id2: number;



  @Input() questions: QuestionModel [] = [];


  constructor(private service: QuizzAPIService) {
  }

  ngOnInit() {
    this.service.getAllQuestions().subscribe(reponse => reponse.forEach(question => {
      this.questions.push(question);
    }));

  }

  clic(id) {
    this.show = true;
    this.id2 = id;
  }

  // @ts-ignore
  confirmer() {
    this.service.deleteQuestion(this.id2).subscribe(() => {

      this.service.getAllQuestions().subscribe(reponse => {this.questions = reponse; }
      ); } );

    this.show = false;

  }

  annuler() {
    this.show = false;
  }
}
