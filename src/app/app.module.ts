import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CreateQuestionComponent } from './create-question/create-question.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { QuestionComponent } from './question/question.component';
import { ListQuestionComponent } from './list-question/list-question.component';
import {HttpClientModule} from '@angular/common/http';
import {QuizzAPIService} from './service/quizzAPIService';
import {RouterModule, Routes} from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { AdminComponent } from './admin/admin.component';
import { DeleteComponent } from './delete/delete.component';
import { QuizzCreationComponent } from './quizz-creation/quizz-creation.component';

const appRoutes: Routes = [
  { path: 'questions', component: ListQuestionComponent },
  { path: 'questions/create',      component: CreateQuestionComponent },
  { path: 'accueil',      component: AccueilComponent },
  { path: 'admin',      component: AdminComponent },
  { path: 'quizzCreator',      component: QuizzCreationComponent },
  { path: 'questions/delete',      component: DeleteComponent }, ];


@NgModule({
  declarations: [
    AppComponent,
    CreateQuestionComponent,
    QuestionComponent,
    ListQuestionComponent,
    AccueilComponent,
    AdminComponent,
    DeleteComponent,
    QuizzCreationComponent,


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes),
    FormsModule
  ],
  providers: [QuizzAPIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
