import {Component, OnInit} from '@angular/core';
import {QuizzAPIService} from '../service/quizzAPIService';
import {BodyCreate} from '../model/bodyCreate';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {

  show = false;
  showgenerator = false;
  difficulty: string;
  theme: string;
  showcreation = false;
  type: string;
  disabled = 'disabled';
  creationquest: string;
  creationrep: string;
  creationrep2: string;
  creationrep3: string;

  constructor(private service: QuizzAPIService) {
  }

  ngOnInit() {
  }

  requeteCreation(id) {
    this.show = true;
    this.type = id;
  }

  clic() {
    console.log(this.creationquest);
  }
  change() {
    console.log(this.creationquest);
  }
  choix1Man() {
    this.showgenerator = false;
    this.showcreation = true;
  }
  choix1Auto() {
    this.showgenerator = true;
    this.showcreation = false;
  }
  clicAutoDifficulty(diff: string) {
    this.difficulty = diff;
    }
  clicAutoTheme(theme: string) {
    this.theme = theme;
  }
  clicGenerate() {
    const bodycreate = new BodyCreate(this.difficulty, this.theme);
    this.service.postcreate(bodycreate).subscribe();

  }

}
