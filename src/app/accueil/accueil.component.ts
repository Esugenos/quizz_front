import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
showpasswd = false;
inactive = 'inactive';
motdepasse = 'admin';
inputpassword: string;
disabled = 'disabled';
  constructor() {


  }

  ngOnInit() {
  }
  confirmer() {
    if (this.inputpassword === this.motdepasse) {      this.disabled = ''; }


  }
  password() {
    this.showpasswd = true;
  }
  annuler() {
    this.showpasswd = false;
  }

}
