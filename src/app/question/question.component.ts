import {Component, Input, OnInit} from '@angular/core';
import {QuizzAPIService} from '../service/quizzAPIService';
import {QuestionModel} from '../model/questionModel';
import {BodyUserAnswer} from '../model/bodyUserAnswer';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  @Input() enonce: string;
  @Input() index: number;
  questions: QuestionModel [] = [];
  @Input() question: QuestionModel;
  @Input() resultLabel: string;
  textAnswer = '';
  show = false;
  disabled = 'disabled';
  disabled2 = '';

  constructor(private service: QuizzAPIService) {
  }

  ngOnInit() {
    this.service.getAllQuestions().subscribe(listquestions => {
      this.questions = listquestions;

    });

  }

    // @ts-ignore
    clic(id) {

      const bodyUserAnswer = new BodyUserAnswer(this.textAnswer);
      this.show = true;
      this.service.postAnswer(id, bodyUserAnswer).subscribe();
      this.service.postAnswer(id, bodyUserAnswer).subscribe(plop => {
        this.resultLabel = plop.result;
      });
    }

    clicTF(id, reponse: string ) {

      const bodyUserAnswer = new BodyUserAnswer(reponse);
      this.show = true;
      this.disabled2 = 'disabled';
      this.service.postAnswer(id, bodyUserAnswer).subscribe();
      this.service.postAnswer(id, bodyUserAnswer).subscribe(plop => {
        this.resultLabel = plop.result;
      });
    }
  clicM(id, reponse: string ) {

    const bodyUserAnswer = new BodyUserAnswer(reponse);
    this.show = true;
    this.disabled2 = 'disabled';
    this.service.postAnswer(id, bodyUserAnswer).subscribe();
    this.service.postAnswer(id, bodyUserAnswer).subscribe(plop => {
      this.resultLabel = plop.result;
    });
  }

    change() {
      if (this.textAnswer.length > 0) {
        this.disabled = '';
      } else {
        this.disabled = 'disabled';
      }
    }
  }
