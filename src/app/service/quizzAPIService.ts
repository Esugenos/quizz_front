import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {QuestionModel} from '../model/questionModel';
import {ListQuestionModel} from '../model/listQuestionModel';
import {Injectable} from '@angular/core';

// penser à injecter le service dans imports et providers de appmodule
@Injectable()
export class QuizzAPIService {

  constructor(private http: HttpClient) {

  }
  getAllQuestions(): Observable<QuestionModel[]> {
return this.http.get<QuestionModel[]>('http://localhost:8080/questions');
  }
  postAnswer(id, body): Observable<QuestionModel> {
    return this.http.post<QuestionModel>(`http://localhost:8080/questions/${id}/:tryanswer`, body);
  }
  postcreate(body): Observable<QuestionModel> {
    return this.http.post<QuestionModel>('http://localhost:8080/questions/create', body);
  }
  deleteQuestion(id): Observable<any> {
    return this.http.delete(`http://localhost:8080/questions/${id}`);
  }
}

