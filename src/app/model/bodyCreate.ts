

export class BodyCreate {

  difficulty: string;
  theme: string
  constructor(difficulty: string, theme: string) {
    this.difficulty = difficulty;
    this.theme = theme;
  }
}
