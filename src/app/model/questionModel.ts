
export class QuestionModel {

  displayableText: string;
  id: number;
  userAnswer: string;
  result: string;
  type: string;
  suggestions: string [];

  constructor(displayableText: string, id: number, userAnswer: string, result: string, type: string, suggestions: string []) {
    this.displayableText = displayableText;
    this.id = id;
    this.userAnswer = userAnswer;
    this.result = result;
    this.type = type;
    this.suggestions = suggestions;
  }
}
