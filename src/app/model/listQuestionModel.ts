import {QuestionModel} from '../model/questionModel';


export class ListQuestionModel {

  questions: QuestionModel [];

  constructor(questions) {
    this.questions = questions;
  }
}
