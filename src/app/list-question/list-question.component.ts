import {Component, Input, OnInit, Output} from '@angular/core';
import {QuestionModel} from '../model/questionModel';
import {QuizzAPIService} from '../service/quizzAPIService';


@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.css']
})
export class ListQuestionComponent implements OnInit {

  questions: QuestionModel [] = [];



  constructor(private service: QuizzAPIService) {
  }

  ngOnInit() {
    this.service.getAllQuestions().subscribe(listQuestion => {
      this.questions = listQuestion;
      this.questions.forEach(question => {
        if (question.displayableText.includes('VRAI')) {
          question.type = 'TRUEFALSE';
        } else if (question.displayableText.includes('Vos choix:')) {
          question.type = 'MULTIPLE';
        } else {
          question.type = 'OPEN';
        }
      });
    });

  }

}
